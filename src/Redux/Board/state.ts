export type IRootState = {
  board: Array<Array<string>>,
  currentPlayer: 'redPlayer' | 'blackPlayer',
  isGameOver: boolean,
  winner: 'R' | 'B' | null
  // selectedChess: boolean,
  // currentChess: { name: string, x: number, y: number } | null
}

export const initialState:IRootState = {
  board: [
    ['cB', 'hB', 'eB', 'jB', 'kB', 'jB', 'eB', 'hB', 'cB'],
    ['', '', '', '', '', '', '', '', ''],
    ['', 'pB', '', '', '', '', '', 'pB', ''],
    ['sB', '', 'sB', '', 'sB', '', 'sB', '', 'sB'],
    ['', '', '', '', '', '', '', '', ''],
    ['', '', '', '', '', '', '', '', ''],
    ['sR', '', 'sR', '', 'sR', '', 'sR', '', 'sR'],
    ['', 'pR', '', '', '', '', '', 'pR', ''],
    ['', '', '', '', '', '', '', '', ''],
    ['cR', 'hR', 'eR', 'jR', 'kR', 'jR', 'eR', 'hR', 'cR']
  ],
  currentPlayer: 'redPlayer',
  isGameOver: false,
  winner: null
  // selectedChess: false,
  // currentChess: { name: '', x: 0, y: 0 }
}
