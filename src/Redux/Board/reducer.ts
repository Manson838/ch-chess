import { IRootAction } from './action';
import { initialState, IRootState } from './state';


export const rootReducer = (state: IRootState = initialState, action: IRootAction): IRootState => {
  switch (action.type) {
    case '@@sStep': {
      let sStep = [...state.board]
      let sY_1 = action.y - 1
      let sYx1 = action.y + 1
      let sX_1 = action.x - 1
      let sXx1 = action.x + 1

      let sAllies = action.name.slice(-1)
      let sEnemy = ''
      if (sAllies === 'R') {
        sEnemy = 'B'
      } else {
        sEnemy = 'R'
      }

      if (sAllies === 'R') {
        if (action.y > 4) {
          if (sStep[sY_1][action.x] === '') {
            sStep[sY_1][action.x] = 'green'
          } else if (sStep[sY_1][action.x].slice(-1) === sEnemy) {
            sStep[sY_1][action.x] = sStep[sY_1][action.x] + 'A'
          }
        } else if (action.y <= 4) {
          if (action.y > 0) {
            if (sStep[sY_1][action.x] === '') {
              sStep[sY_1][action.x] = 'green'
            } else if (sStep[sY_1][action.x].slice(-1) === sEnemy) {
              sStep[sY_1][action.x] = sStep[sY_1][action.x] + 'A'
            }
          }

          if (action.x > 0) {
            if (sStep[action.y][sX_1] === '') {
              sStep[action.y][sX_1] = 'green'
            } else if (sStep[action.y][sX_1].slice(-1) === sEnemy) {
              sStep[action.y][sX_1] = sStep[action.y][sX_1] + 'A'
            }
          }

          if (action.x < 8) {
            if (sStep[action.y][sXx1] === '') {
              sStep[action.y][sXx1] = 'green'
            } else if (sStep[action.y][sXx1].slice(-1) === sEnemy) {
              sStep[action.y][sXx1] = sStep[action.y][sXx1] + 'A'
            }
          }
        }
      } else if (sAllies === 'B') {
        if (action.y < 5) {
          if (sStep[sYx1][action.x] === '') {
            sStep[sYx1][action.x] = 'green'
          } else if (sStep[sYx1][action.x].slice(-1) === sEnemy) {
            sStep[sYx1][action.x] = sStep[sYx1][action.x] + 'A'
          }
        } else if (action.y >= 5) {
          if (action.y < 9) {
            if (sStep[sYx1][action.x] === '') {
              sStep[sYx1][action.x] = 'green'
            } else if (sStep[sYx1][action.x].slice(-1) === sEnemy) {
              sStep[sYx1][action.x] = sStep[sYx1][action.x] + 'A'
            }

            if (action.x > 0) {
              if (sStep[action.y][sX_1] === '') {
                sStep[action.y][sX_1] = 'green'
              } else if (sStep[action.y][sX_1].slice(-1) === sEnemy) {
                sStep[action.y][sX_1] = sStep[action.y][sX_1] + 'A'
              }
            }

            if (action.x < 8) {
              if (sStep[action.y][sXx1] === '') {
                sStep[action.y][sXx1] = 'green'
              } else if (sStep[action.y][sXx1].slice(-1) === sEnemy) {
                sStep[action.y][sXx1] = sStep[action.y][sXx1] + 'A'
              }
            }
          }
        }
      }

      return { ...state, board: sStep }
    }

    case '@@pStep': {
      let pStep = [...state.board]
      let pAllies = action.name.slice(-1)
      let pEnemy = ''
      if (pAllies === 'R') {
        pEnemy = 'B'
      } else {
        pEnemy = 'R'
      }

      console.log();
      
      //up
      for (let i = action.y - 1; i >= 0; i--) {
        if (pStep[i][action.x] === '') {
          pStep[i][action.x] = 'green'
        } else if (pStep[i][action.x].length === 2 && i !== 0) {
          let barrier = i - 1
          for (let attackArea = barrier; attackArea > -1; attackArea--) {
            if (pStep[attackArea][action.x].length === 2 && pStep[attackArea][action.x].slice(-1) === pAllies) {
              break
            } else if (pStep[attackArea][action.x].length === 2 && pStep[attackArea][action.x].slice(-1) === pEnemy) {
              pStep[attackArea][action.x] = pStep[attackArea][action.x] + 'A'
              break
            }
          }
          break
        }
      }

      //down
      for (let j = action.y + 1; j < 10; j++) {
        if (pStep[j][action.x] === '') {
          pStep[j][action.x] = 'green'
        } else if (pStep[j][action.x].length === 2 && j !== 9) {
          let barrier = j + 1
          for (let attackArea = barrier; attackArea < 10; attackArea++) {
            if (pStep[attackArea][action.x].length === 2 && pStep[attackArea][action.x].slice(-1) === pAllies) {
              break
            } else if (pStep[attackArea][action.x].length === 2 && pStep[attackArea][action.x].slice(-1) === pEnemy) {
              pStep[attackArea][action.x] = pStep[attackArea][action.x] + 'A'
              break
            }
          }
          break
        }
      }

      //left
      for (let k = action.x - 1; k >= 0; k--) {
        if (pStep[action.y][k] === '') {
          pStep[action.y][k] = 'green'
        } else if (pStep[action.y][k].length === 2 && k !== 0) {
          let barrier = k - 1
          for (let attackArea = barrier; attackArea > -1; attackArea--) {
            if (pStep[action.y][attackArea].length === 2 && pStep[action.y][attackArea].slice(-1) === pAllies) {
              break
            } else if (pStep[action.y][attackArea].length === 2 && pStep[action.y][attackArea].slice(-1) === pEnemy) {
              pStep[action.y][attackArea] = pStep[action.y][attackArea] + 'A'
              break
            }
          }
          break
        }
      }

      //right
      for (let l = action.x + 1; l < 9; l++) {
        if (pStep[action.y][l] === '') {
          pStep[action.y][l] = 'green'
        } else if (pStep[action.y][l].length === 2 && l !== 8) {
          let barrier = l + 1
          for (let attackArea = barrier; attackArea < 9; attackArea++) {
            if (pStep[action.y][attackArea].length === 2 && pStep[action.y][attackArea].slice(-1) === pAllies) {
              break
            } else if (pStep[action.y][attackArea].length === 2 && pStep[action.y][attackArea].slice(-1) === pEnemy) {
              pStep[action.y][attackArea] = pStep[action.y][attackArea] + 'A'
              break
            }
          }
          break
        }
      }

      return { ...state, board: pStep }
    }

    case '@@cStep': {
      let cStep = [...state.board]
      let cAllies = action.name.slice(-1)
      let cEnemy = ''
      if (cAllies === 'R') {
        cEnemy = 'B'
      } else {
        cEnemy = 'R'
      }

      //up
      for (let i = action.y - 1; i >= 0; i--) {
        if (cStep[i][action.x] === '') {
          cStep[i][action.x] = 'green'
        } else if (cStep[i][action.x].length === 2 && cStep[i][action.x].slice(-1) === cAllies) {
          break
        } else if (cStep[i][action.x].length === 2 && cStep[i][action.x].slice(-1) === cEnemy) {
          cStep[i][action.x] = cStep[i][action.x] + 'A'
          break
        }
      }
      //down
      for (let i = action.y + 1; i < 10; i++) {
        if (cStep[i][action.x] === '') {
          cStep[i][action.x] = 'green'
        } else if (cStep[i][action.x].length === 2 && cStep[i][action.x].slice(-1) === cAllies) {
          break
        } else if (cStep[i][action.x].length === 2 && cStep[i][action.x].slice(-1) === cEnemy) {
          cStep[i][action.x] = cStep[i][action.x] + 'A'
          break
        }
      }
      //left
      for (let i = action.x - 1; i >= 0; i--) {
        if (cStep[action.y][i] === '') {
          cStep[action.y][i] = 'green'
        } else if (cStep[action.y][i].length === 2 && cStep[action.y][i].slice(-1) === cAllies) {
          break
        } else if (cStep[action.y][i].length === 2 && cStep[action.y][i].slice(-1) === cEnemy) {
          cStep[action.y][i] = cStep[action.y][i] + 'A'
          break
        }
      }
      //right
      for (let i = action.x + 1; i < 9; i++) {
        if (cStep[action.y][i] === '') {
          cStep[action.y][i] = 'green'
        } else if (cStep[action.y][i].length === 2 && cStep[action.y][i].slice(-1) === cAllies) {
          break
        } else if (cStep[action.y][i].length === 2 && cStep[action.y][i].slice(-1) === cEnemy) {
          cStep[action.y][i] = cStep[action.y][i] + 'A'
          break
        }
      }
      return { ...state, board: cStep }
    }

    case '@@hStep': {
      let hStep = [...state.board]
      let hEnemy = ''
      if (action.name.slice(-1) === 'R') {
        hEnemy = 'B'
      } else {
        hEnemy = 'R'
      }
      let hY_2 = action.y - 2
      let hY_1 = action.y - 1
      let hYx2 = action.y + 2
      let hYx1 = action.y + 1
      let hY = action.y
      let hX_2 = action.x - 2
      let hX_1 = action.x - 1
      let hXx2 = action.x + 2
      let hXx1 = action.x + 1
      let hX = action.x

      //up
      if (hY_2 >= 0 && hStep[hY_1][hX] === '') {
        if (hX_1 >= 0 && hStep[hY_2][hX_1] === '') {
          hStep[hY_2][hX_1] = 'green'
        } else if (hX_2 >= 0 && hStep[hY_2][hX_1].slice(-1) === hEnemy) {
          hStep[hY_2][hX_1] = hStep[hY_2][hX_1] + 'A'
        }

        if (hXx1 < 9 && hStep[hY_2][hXx1] === '') {
          hStep[hY_2][hXx1] = 'green'
        } else if (hXx1 < 9 && hStep[hY_2][hXx1].slice(-1) === hEnemy) {
          hStep[hY_2][hXx1] = hStep[hY_2][hXx1] + 'A'
        }
      }
      //down
      if (hYx2 < 10 && hStep[hYx1][hX] === '') {
        if (hX_1 >= 0 && hStep[hYx2][hX_1] === '') {
          hStep[hYx2][hX_1] = 'green'
        } else if (hX_1 >= 0 && hStep[hYx2][hX_1].slice(-1) === hEnemy) {
          hStep[hYx2][hX_1] = hStep[hYx2][hX_1] + 'A'
        }

        if (hXx1 < 9 && hStep[hYx2][hXx1] === '') {
          hStep[hYx2][hXx1] = 'green'
        } else if (hXx1 < 9 && hStep[hYx2][hXx1].slice(-1) === hEnemy) {
          hStep[hYx2][hXx1] = hStep[hYx2][hXx1] + 'A'
        }
      }
      //left
      if (hX_2 >= 0 && hStep[hY][hX_1] === '') {
        if (hY_1 >= 0 && hStep[hY_1][hX_2] === '') {
          hStep[hY_1][hX_2] = 'green'
        } else if (hY_1 >= 0 && hStep[hY_1][hX_2].slice(-1) === hEnemy) {
          hStep[hY_1][hX_2] = hStep[hY_1][hX_2] + 'A'
        }

        if (hYx1 < 10 && hStep[hYx1][hX_2] === '') {
          hStep[hYx1][hX_2] = 'green'
        } else if (hYx1 < 10 && hStep[hYx1][hX_2].slice(-1) === hEnemy) {
          hStep[hYx1][hX_2] = hStep[hYx1][hX_2] + 'A'
        }
      }
      //right
      if (hXx2 < 9 && hStep[hY][hXx1] === '') {
        if (hY_1 >= 0 && hStep[hY_1][hXx2] === '') {
          hStep[hY_1][hXx2] = 'green'
        } else if (hY_1 >= 0 && hStep[hY_1][hXx2].slice(-1) === hEnemy) {
          console.log();
          
          hStep[hY_1][hXx2] = hStep[hY_1][hXx2] + 'A'
        }

        if (hYx1 < 10 && hStep[hYx1][hXx2] === '') {
          hStep[hYx1][hXx2] = 'green'
        } else if (hYx1 < 10 && hStep[hYx1][hXx2].slice(-1) === hEnemy) {
          hStep[hYx1][hXx2] = hStep[hYx1][hXx2] + 'A'
        }
      }


      return { ...state, board: hStep }
    }

    case '@@eStep': {
      let eStep = [...state.board]
      let eAllies = action.name.slice(-1)
      let eEnemy = ''
      if (eAllies === 'R') {
        eEnemy = 'B'
      } else {
        eEnemy = 'R'
      }

      let eY_2 = action.y - 2
      let eY_1 = action.y - 1
      let eYx2 = action.y + 2
      let eYx1 = action.y + 1
      let eY = action.y
      let eX_2 = action.x - 2
      let eX_1 = action.x - 1
      let eXx2 = action.x + 2
      let eXx1 = action.x + 1
      let eX = action.x

      if (eAllies === 'R') {
        if (eStep[eY_1][eX_1] === '' && eY > 5) {
          if (eStep[eY_2][eX_2] === '') {
            eStep[eY_2][eX_2] = 'green'
          } else if (eStep[eY_2][eX_2].slice(-1) === eEnemy) {
            eStep[eY_2][eX_2] = eStep[eY_2][eX_2] + 'A'
          }
        }
        if (eStep[eY_1][eXx1] === '' && eY > 5) {
          if (eStep[eY_2][eXx2] === '') {
            eStep[eY_2][eXx2] = 'green'
          } else if (eStep[eY_2][eXx2].slice(-1) === eEnemy) {
            eStep[eY_2][eXx2] = eStep[eY_2][eXx2] + 'A'
          }
        }
        if (eY < 9) {
          if (eStep[eYx1][eX_1] === '') {
            if (eStep[eYx2][eX_2] === '') {
              eStep[eYx2][eX_2] = 'green'
            } else if (eStep[eYx2][eX_2].slice(-1) === eEnemy) {
              eStep[eYx2][eX_2] = eStep[eYx2][eX_2] + 'A'
            }
          }

          if (eStep[eYx1][eXx1] === '') {
            if (eStep[eYx2][eXx2] === '') {
              eStep[eYx2][eXx2] = 'green'
            } else if (eStep[eYx2][eXx2].slice(-1) === eEnemy) {
              eStep[eYx2][eXx2] = eStep[eYx2][eXx2] + 'A'
            }
          }
        }
      } else if (eAllies === 'B') {
        if (eY > 0) {
          if (eStep[eY_1][eX_1] === '') {
            if (eStep[eY_2][eX_2] === '') {
              eStep[eY_2][eX_2] = 'green'
            } else if (eStep[eY_2][eX_2].slice(-1) === eEnemy) {
              eStep[eY_2][eX_2] = eStep[eY_2][eX_2] + 'A'
            }
          }
          if (eStep[eY_1][eXx1] === '') {
            if (eStep[eY_2][eXx2] === '') {
              eStep[eY_2][eXx2] = 'green'
            } else if (eStep[eY_2][eXx2].slice(-1) === eEnemy) {
              eStep[eY_2][eXx2] = eStep[eY_2][eXx2] + 'A'
            }
          }
        }
        if (eY < 5) {
          if (eStep[eYx1][eX_1] === '' && eY < 4) {
            if (eStep[eYx2][eX_2] === '') {
              eStep[eYx2][eX_2] = 'green'
            } else if (eStep[eYx2][eX_2].slice(-1) === eEnemy) {
              eStep[eYx2][eX_2] = eStep[eYx2][eX_2] + 'A'
            }
          }

          if (eStep[eYx1][eXx1] === '' && eY < 4) {
            if (eStep[eYx2][eXx2] === '') {
              eStep[eYx2][eXx2] = 'green'
            } else if (eStep[eYx2][eXx2].slice(-1) === eEnemy) {
              eStep[eYx2][eXx2] = eStep[eYx2][eXx2] + 'A'
            }
          }
        }
      }


      return { ...state, board: eStep }
    }

    case '@@jStep': {
      let jStep = [...state.board]
      let jAllies = action.name.slice(-1)
      let jEnemy = ''
      if (jAllies === 'R') {
        jEnemy = 'B'
      } else {
        jEnemy = 'R'
      }
      let jY_1 = action.y - 1
      let jYx1 = action.y + 1
      let jY = action.y
      let jX_1 = action.x - 1
      let jXx1 = action.x + 1
      let jX = action.x

      if (jAllies === 'R') {
        if (jX !== 3) {
          if (jY !== 7) {
            if (jStep[jY_1][jX_1] === '') {
              jStep[jY_1][jX_1] = 'green'
            } else if (jStep[jY_1][jX_1].slice(-1) === jEnemy) {
              jStep[jY_1][jX_1] = jStep[jY_1][jX_1] + 'A'
            }
          }

          if (jY !== 9) {
            if (jStep[jYx1][jX_1] === '') {
              jStep[jYx1][jX_1] = 'green'
            } else if (jStep[jYx1][jX_1].slice(-1) === jEnemy) {
              jStep[jYx1][jX_1] = jStep[jYx1][jX_1] + 'A'
            }
          }
        }
        if (jX !== 5) {
          if (jY !== 7) {
            if (jStep[jY_1][jXx1] === '') {
              jStep[jY_1][jXx1] = 'green'
            } else if (jStep[jY_1][jXx1].slice(-1) === jEnemy) {
              jStep[jY_1][jXx1] = jStep[jY_1][jXx1] + 'A'
            }
          }

          if (jY !== 9) {
            if (jStep[jYx1][jXx1] === '') {
              jStep[jYx1][jXx1] = 'green'
            } else if (jStep[jYx1][jXx1].slice(-1) === jEnemy) {
              jStep[jYx1][jXx1] = jStep[jYx1][jXx1] + 'A'
            }
          }
        }
      } else if (jAllies === 'B') {
        if (jX !== 3) {
          if (jY !== 0) {
            if (jStep[jY_1][jX_1] === '') {
              jStep[jY_1][jX_1] = 'green'
            } else if (jStep[jY_1][jX_1].slice(-1) === jEnemy) {
              jStep[jY_1][jX_1] = jStep[jY_1][jX_1] + 'A'
            }
          }

          if (jY !== 2) {
            if (jStep[jYx1][jX_1] === '') {
              jStep[jYx1][jX_1] = 'green'
            } else if (jStep[jYx1][jX_1].slice(-1) === jEnemy) {
              jStep[jYx1][jX_1] = jStep[jYx1][jX_1] + 'A'
            }
          }
        }
        if (jX !== 5) {
          if (jY !== 0) {
            if (jStep[jY_1][jXx1] === '') {
              jStep[jY_1][jXx1] = 'green'
            } else if (jStep[jY_1][jXx1].slice(-1) === jEnemy) {
              jStep[jY_1][jXx1] = jStep[jY_1][jXx1] + 'A'
            }
          }

          if (jY !== 2) {
            if (jStep[jYx1][jXx1] === '') {
              jStep[jYx1][jXx1] = 'green'
            } else if (jStep[jYx1][jXx1].slice(-1) === jEnemy) {
              jStep[jYx1][jXx1] = jStep[jYx1][jXx1] + 'A'
            }
          }
        }
      }

      return {...state, board: jStep}
    }

    case '@@kStep': {
      let kStep = [...state.board]
      let kAllies = action.name.slice(-1)
      let kEnemy = ''
      if (kAllies === 'R') {
        kEnemy = 'B'
      } else {
        kEnemy = 'R'
      }
      let kY_1 = action.y - 1
      let kYx1 = action.y + 1
      let kY = action.y
      let kX_1 = action.x - 1
      let kXx1 = action.x + 1
      let kX = action.x

      if (kAllies === 'R') {
          if (kY !== 7) {
            if (kStep[kY_1][kX] === '') {
              kStep[kY_1][kX] = 'green'
            } else if (kStep[kY_1][kX].slice(-1) === kEnemy) {
              kStep[kY_1][kX] = kStep[kY_1][kX] + 'A'
            }
          }

          if (kY !== 9) {
            if (kStep[kYx1][kX] === '') {
              kStep[kYx1][kX] = 'green'
            } else if (kStep[kYx1][kX].slice(-1) === kEnemy) {
              kStep[kYx1][kX] = kStep[kYx1][kX] + 'A'
            }
          }
        

          if (kX !== 5) {
            if (kStep[kY][kXx1] === '') {
              kStep[kY][kXx1] = 'green'
            } else if (kStep[kY][kXx1].slice(-1) === kEnemy) {
              kStep[kY][kXx1] = kStep[kY][kXx1] + 'A'
            }
          }

          if (kX !== 3) {
            if (kStep[kY][kX_1] === '') {
              kStep[kY][kX_1] = 'green'
            } else if (kStep[kY][kX_1].slice(-1) === kEnemy) {
              kStep[kY][kX_1] = kStep[kY][kX_1] + 'A'
            }
          
        }
      } else if (kAllies === 'B') {
        if (kY !== 0) {
          if (kStep[kY_1][kX] === '') {
            kStep[kY_1][kX] = 'green'
          } else if (kStep[kY_1][kX].slice(-1) === kEnemy) {
            kStep[kY_1][kX] = kStep[kY_1][kX] + 'A'
          }
        }

        if (kY !== 2) {
          if (kStep[kYx1][kX] === '') {
            kStep[kYx1][kX] = 'green'
          } else if (kStep[kYx1][kX].slice(-1) === kEnemy) {
            kStep[kYx1][kX] = kStep[kYx1][kX] + 'A'
          }
        }
      

        if (kX !== 5) {
          if (kStep[kY][kXx1] === '') {
            kStep[kY][kXx1] = 'green'
          } else if (kStep[kY][kXx1].slice(-1) === kEnemy) {
            kStep[kY][kXx1] = kStep[kY][kXx1] + 'A'
          }
        }

        if (kX !== 3) {
          if (kStep[kY][kX_1] === '') {
            kStep[kY][kX_1] = 'green'
          } else if (kStep[kY][kX_1].slice(-1) === kEnemy) {
            kStep[kY][kX_1] = kStep[kY][kX_1] + 'A'
          }
        
      }
    }

      return {...state, board: kStep}
    }
      
    case '@@moveChess': {
      let moveChess = [...state.board]
      let nextPlayer = state.currentPlayer
      let winner = checkWinning(moveChess)

      moveChess[action.y][action.x] = action.chess
      moveChess[action.currentY][action.currentX] = ''

      for (let y = 0; y < 10; y++) {
        for (let x = 0; x < 9; x++) {
          if (moveChess[y][x] === 'green') {
            moveChess[y][x] = ''
          } else if (moveChess[y][x].length === 3) {
            moveChess[y][x] = moveChess[y][x].slice(0, -1)
          }
        }
      }
      localStorage.setItem('board', JSON.stringify(moveChess))

      if (winner === 'B') {
        return { ...state, board: moveChess, isGameOver: true, winner: 'B' }
      } else if (winner === 'R') {
        return { ...state, board: moveChess, isGameOver: true, winner: 'R' }
      }
      
      // changing currentPlayer
      if (state.currentPlayer === 'redPlayer') {
        nextPlayer = 'blackPlayer'
      } else {
        nextPlayer = 'redPlayer'
      }

      return { ...state, board: moveChess, currentPlayer: nextPlayer }
    }

    case '@@clearGreen': {
      let clearGreen = [...state.board]
      for (let y = 0; y < 10; y++) {
        for (let x = 0; x < 9; x++) {
          if (clearGreen[y][x] === 'green') {
            clearGreen[y][x] = ''
          } else if (clearGreen[y][x].length === 3) {
            clearGreen[y][x] = clearGreen[y][x].slice(0, -1)
          }
        }
      }
      return { ...state, board: clearGreen }
    }

    default:
      return state
  }
}

function checkWinning(board: Array<Array<string>>) {
  let winnerCheck = 0
  for (let y = 0; y < 3; y++) {
    for (let x = 3; x < 6; x++) {
      if (board[y][x] === 'kB') {
        winnerCheck += 1
        break
      }
    }
  }

  for (let y = 7; y < 10; y++) {
    for (let x = 3; x < 6; x++) {
      if (board[y][x] === 'kR') {
        winnerCheck -= 1
        break
      }
    }
  }

  if (winnerCheck === 0) {
    return
  } else if (winnerCheck > 0) {
    return 'B'
  } else if (winnerCheck < 0) {
    return 'R'
  }
}