export function sStep(x: number, y: number, name: 'sR' | 'sB') {
  return {
    type: '@@sStep' as const,
    x,
    y,
    name
  }
}
export function pStep(x: number, y: number, name: 'pR' | 'pB') {
  return {
    type: '@@pStep' as const,
    x,
    y,
    name
  }
}
export function cStep(x: number, y: number, name: 'cR' | 'cB') {
  return {
    type: '@@cStep' as const,
    x,
    y,
    name
  }
}
export function hStep(x: number, y: number, name: 'hR' | 'hB') {
  return {
    type: '@@hStep' as const,
    x,
    y,
    name
  }
}
export function eStep(x: number, y: number, name: 'eR' | 'eB') {
  return {
    type: '@@eStep' as const,
    x,
    y,
    name
  }
}
export function jStep(x: number, y: number, name: 'jR' | 'jB') {
  return {
    type: '@@jStep' as const,
    x,
    y,
    name
  }
}
export function kStep(x: number, y: number, name: 'kR' | 'kB') {
  return {
    type: '@@kStep' as const,
    x,
    y,
    name
  }
}

export function moveChess(x: number, y: number, chess: string, currentX: number, currentY: number) {
  return {
    type: '@@moveChess' as const,
    x,
    y,
    chess,
    currentX,
    currentY
  }
}

export function clearGreen() {
  return {
    type: '@@clearGreen' as const,
  }
}

export type IRootAction = 
  | ReturnType<typeof sStep>
  | ReturnType<typeof pStep>
  | ReturnType<typeof cStep>
  | ReturnType<typeof hStep>
  | ReturnType<typeof eStep>
  | ReturnType<typeof jStep>
  | ReturnType<typeof kStep>
  | ReturnType<typeof moveChess>
  | ReturnType<typeof clearGreen>
