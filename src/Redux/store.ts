import { IRootAction } from './Board/action';
import { IRootState } from './Board/state';
import { rootReducer } from './Board/reducer';
import { createStore, compose, applyMiddleware } from "redux";
import logger from 'redux-logger';

declare global {
  /* tslint:disable:interface-name */
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any
  }
}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;


const store = createStore<IRootState, IRootAction, {}, {}>(
  rootReducer,
  composeEnhancers(
    applyMiddleware(logger)
  )
);
export default store;