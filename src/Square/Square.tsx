import './Square.css';

export type ISquareProps = {
  id: {x:number, y:number}
  image?: string | null
  name?: string
  onSquareClick: () => void
  beAttacked?: boolean
}

function Square(props: ISquareProps) {
  let boardSquare = <></>

  let id = 's_' + props.id.x + '_' + props.id.y
  if (!props.beAttacked && props.image) {
    boardSquare = (
      <div className="square" onClick={() => props.onSquareClick()} id={id}>
        <img src={props.image} alt="red solider" className="chess" />
      </div>
    )
  } else if (props.beAttacked && props.image) {
    boardSquare = (
      <div className="square" onClick={() => props.onSquareClick()} id={id}>
        <img src={'green.png'} alt="red solider" className="atkPrd" />
        <img src={props.image} alt="red solider" className="chess" />
      </div>
    )
  } else {
    boardSquare = (
      <div className="square" onClick={() => props.onSquareClick()} id={id}>
      </div>
    )
  }

  return boardSquare;
}

export default Square;

