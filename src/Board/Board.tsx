// import React from 'react';
import Square from '../Square/Square';
import './Board.css';
import { useDispatch, useSelector } from 'react-redux';
import { sStep, moveChess, clearGreen, pStep, cStep, hStep, eStep, jStep, kStep } from '../Redux/Board/action';
import { IRootState } from '../Redux/Board/state'
import { useState } from 'react';
// import { useEffect } from 'react';



function Board() {
  const dispatch = useDispatch()

  const [selectedMode, setSelectedMode] = useState(false)
  const [currentName, setCurrentName] = useState('')
  const [currentX, setCurrentX] = useState(-1)
  const [currentY, setCurrentY] = useState(-1)

  const board = useSelector((state: IRootState) => state.board)
  const currentPlayer = useSelector((state: IRootState) => state.currentPlayer)
  const isGameOver = useSelector((state: IRootState) => state.isGameOver)

  console.log({ currentPlayer });

  function clickPlay(x: number, y: number) {
    let name = board[y][x]
 
    console.log({ selectedMode });

    if (isGameOver) {
      return
    }

    if (selectedMode === false) {
      console.log({ name, x, y }, selectedMode)
      if (currentPlayer === 'redPlayer') {
        switch (name) {
          case 'sR':
            chessPicked(x, y, name)
            dispatch(sStep(x, y, name))
            break;
          case 'pR':
            chessPicked(x, y, name)
            dispatch(pStep(x, y, name))
            break;
          case 'cR':
            chessPicked(x, y, name)
            dispatch(cStep(x, y, name))
            break;
          case 'hR':
            chessPicked(x, y, name)
            dispatch(hStep(x, y, name))
            break;
          case 'eR':
            chessPicked(x, y, name)
            dispatch(eStep(x, y, name))
            break;
          case 'jR':
            chessPicked(x, y, name)
            dispatch(jStep(x, y, name))
            break;
          case 'kR':
            chessPicked(x, y, name)
            dispatch(kStep(x, y, name))
            break;
        }
      } else if (currentPlayer === 'blackPlayer') {
        switch (name) {
          case 'sB':
            chessPicked(x, y, name)
            dispatch(sStep(x, y, name))
            break;
          case 'pB':
            chessPicked(x, y, name)
            dispatch(pStep(x, y, name))
            break;
          case 'cB':
            chessPicked(x, y, name)
            dispatch(cStep(x, y, name))
            break;
          case 'hB':
            chessPicked(x, y, name)
            dispatch(hStep(x, y, name))
            break;
          case 'eB':
            chessPicked(x, y, name)
            dispatch(eStep(x, y, name))
            break;
          case 'jB':
            chessPicked(x, y, name)
            dispatch(jStep(x, y, name))
            break;
          case 'kB':
            chessPicked(x, y, name)
            dispatch(kStep(x, y, name))
            break;
        }
      }
    } else if (selectedMode === true) {

      if (name === 'green') {
        dispatch(moveChess(x, y, currentName, currentX, currentY))

        setSelectedMode(false)
        setCurrentName('')

        console.log('move: ', board[y][x]);
      } else if (name.length === 3) {
        dispatch(moveChess(x, y, currentName, currentX, currentY))

        setSelectedMode(false)
        setCurrentName('')

      } else if (name === '' || name.length === 2) {
        console.log({ selectedMode });

        dispatch(clearGreen())
        setSelectedMode(false)
        setCurrentName('')
      }
    }

  }

  function chessPicked(x: number, y: number, name: string) {
    setCurrentX(x)
    setCurrentY(y)
    setSelectedMode(true)
    setCurrentName(name)
  }

  function createBoard() {
    let rows = []
    let id = 0
    for (let y = 0; y < 10; y++) {
      let row = []
      for (let x = 0; x < 9; x++) {
        const squareId = { y, x }
        let chessName = board[y][x]

        let _x = x
        let _y = y

        let col = <></>
        if (board[y][x].length === 2 || board[y][x] === 'green') {
          col = (
            <Square id={squareId} name={chessName} key={id} image={`${chessName}.png`} onSquareClick={() => clickPlay(_x, _y)} />
          )
        } else if (board[y][x].length === 3) {
          let newChess = chessName.slice(0, 2)
          col = (
            <Square id={squareId} name={chessName} key={id} image={`${newChess}.png`} onSquareClick={() => clickPlay(_x, _y)} beAttacked={true} />
          )
        } else {
          col = (
            <Square id={squareId} key={id} onSquareClick={() => clickPlay(_x, _y)} />
          )
        }

        row.push(<div key={id} className='col'>{col}</div>)
        id++
      }
      rows.push(<div key={id} className="row">{row}</div>)
    }
    return (rows)
  }


  return <div className="Board">
    {createBoard()}</div>
}

export default Board;
