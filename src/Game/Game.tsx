import React from 'react';
import Board from '../Board/Board';
import './Game.css';
import { Provider, useSelector } from 'react-redux'

import { IRootState } from '../Redux/Board/state';

function Game() {
  const currentPlayer = useSelector((state: IRootState) => state.currentPlayer)
  const isGameOver = useSelector((state: IRootState) => state.isGameOver)
  const winner = useSelector((state: IRootState) => state.winner)

  console.log({ isGameOver, winner });

  let status = <></>
  let playerIcon = <></>

  if (currentPlayer === 'redPlayer' || winner === 'R') {
    playerIcon = <img src="kR.png" alt="red turn" />
  } else if (currentPlayer === 'blackPlayer' || winner === 'B') {
    playerIcon = <img src="kB.png" alt="black turn" />
    
  }

  if (!isGameOver) {
    status = <div className="status"> Current Player: {playerIcon} </div>
  } else {
    status = <div className="status"> Winner: {playerIcon} </div>
  }

  return (

    <div className='Game'>
      {status}
      <Board />
    </div>

  );
}

export default Game;
