import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import Game from './Game/Game';
import './index.css';
import reportWebVitals from './reportWebVitals';
import store from './Redux/store'

ReactDOM.render(
  <Provider store={store}>
  <React.StrictMode>
    <Game />
    </React.StrictMode>
    </Provider>,
  document.getElementById('root')
);

reportWebVitals();
